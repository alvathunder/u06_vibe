# U06 Viktor 'VIBE' Berg
### This is a document explaining everything you need to know about this project.

#### How to use at the bottom:

The idea with this project is to create a client that saves,
compiles and plays music from a local library on the computer it's running on.
The project is going to be created using python and SQLite, and will talk
to an API on the web to get information about a specific artist, track or
album and then saves that to the audiofile in question.

####1.0 Functions:
- 1.1 Find audiofile locally (.mp3, .wav, etc.) by either entering a path manually,
    or search through the system automatically.
- 1.2 Create playlists with at least two or more songs.
  - 1.3 Ability to sort and filter by:
    - 1.3.1 Artist
    - 1.3.2 Album name
    - 1.3.3 Genre
    - 1.3.4 Album date
- 1.4 Manually trigger library update.
- 1.5 Ability to use cross-platform for both personal use, and development.
- 1.6 Easy to search and find files locally.
- 1.7 Get information about an artist, track or album and save the information 
    to the specific audiofile using 'eyeD3' with python.
- 1.8 Get the information from above and save it to a database to be built on onwards.
- 1.9 The ability to play, pause, rewind and queue songs.


####2.0 Dependencies:
- 2.1 python
- 2.2 PyQt5 (create the GUI, and play songs) (Edited)
- 2.3 pydub (converting and exporting audio files to mp3)
- 2.4 requests (using API to get information about artists etc.)
- 2.5 eyeD3 (load and save information about the track to the audiofile itself)
  - 2.5.1 NOTE: Using windows, you must also pip install 'python-magic-bin'
- 2.6 SQLite3
- 2.7 ffmpeg (Pydub uses ffmpeg to convert audio files to mp3)
- 2.8 ShazamAPI (Finding matches for audio files to get artist, album etc.)

###Sprint 1:
Sprint one will mainly focus on research, finding the best way to load a song 
and play it from a GUI. Finding a good and reliable API to get information about 
artists and songs. The most important part is managing the workflow between 
different platforms and making it work in all environments. Finding a way to search 
through and find audio files no matter what platform you're working on. Here I will 
also create the database model for the app that can be further built on in the future.

#####Functions to focus on:
- 1.1 Find audiofile locally (.mp3, .wav, etc.) by either entering a path manually,
    or search through the system automatically.
- 1.5 Ability to use cross-platform for both personal use, and development.
- 1.6 Easy to search and find files locally.
- 1.7 Get information about an artist, track or album and save the information 
    to the specific audiofile using 'eyeD3' with python.
- 1.8 Get the information from above and save it to a database to be built on onwards.
- 1.9 The ability to play, pause.

###Sprint 2:
Sprint two will focus on fine-tuning and creating functions such as being able to 
sort and filter by artist name, album name, album year and so on. The ability to
create and save playlists will also be implemented in this stage. If there is time, 
this will be the stage where writing information to the audio files about artist, album,
year etc. will be implemented.

#####Functions to focus on:
- 1.2 Create playlists with at least two or more songs.
  - 1.3 Ability to sort and filter by:
    - 1.3.1 Artist
    - 1.3.2 Album name
    - 1.3.3 Genre
    - 1.3.4 Album date
- 1.4 Manually trigger library update.
- 1.9.1 Rewind and queue songs.


### How to use:

### Running on MacOS:
Running this code on MacOS will be a bit more complicated than running it on Linux. While it should be no problem with running it on an intel chip based Mac machine, the M1 chip might not be fully supported. 

- Firstly see to it that you have homebrew installed on your Mac. (If that's not the case,
  go to this link and follow the instructions: https://brew.sh/)
- Once thats done, paste this command in the terminal window of your machine:
  __brew tap avpres/formulae__
- After that is finished, paste this command:
  __brew install --with-openjpeg --with-rubberband --with-tesseract avpres/formulae/ffmpeg__

What this has done is install ffmpeg for your machine to be able to run the script with the 
  abillity to convert audio files to mp3.

Installing dependencies with pip:
- Make sure the project directory is placed in your home folder and rename it to 'PyMono'.
  This is important because the path variables in the code is dependent on this. 
- Inside the project directory, create a virtual environment. NOTE: Python 3.10 has to be 
  installed for this to work! To create a virtual environment with Python 3.10, simply
  type __python3 -m venv qtenv__ (The name of this environment will be 'qtenv', this can be 
  what ever you want, but remember to add the new name to your .gitignore file)
- Activate the virtual environment by typing __source qtenv/bin/activate__ inside the 
  project directory
- Install all the dependencies by typing __pip install -r requirements.txt__

Wait for the install to finish, if any errors occur you need to contact me to get this fixed.
If no errors occur:

- Run /Pymono/src/main.py to initialize a sqlite3 file and automatically trigger
  a library update. (This compares the audio file to the ShazamAPI to find information
  about artist, song, album etc.)
- Once opened, press play or shuffle to start playing.

### Running on Windows:
As of this last update, I have found no way of running this program without errors on 
Windows.

