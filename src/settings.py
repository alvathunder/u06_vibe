from pathlib import Path

WORK_PATH = Path.home() / 'PyMono'
MUSIC = WORK_PATH / 'music'
LIBRARY = MUSIC / 'library'
SONGS_JSON = MUSIC / 'songs_json'
DATABASE = WORK_PATH / 'database'
DB_FILE = DATABASE / 'Pymono.db'
SRC = WORK_PATH / 'src'
MOCK = WORK_PATH / 'mock'
IMG = WORK_PATH / 'img'
LIB_FUNC = MUSIC / 'library_cache'


if __name__ == '__main__':
	pass

