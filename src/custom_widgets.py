from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class ScrollableLabel(QScrollArea):

	def __init__(self, *args, **kwargs):
		QScrollArea.__init__(self, *args, **kwargs)

		self.setWidgetResizable(True)
		self.horizontalScrollBar().setStyleSheet('QScrollBar {height:0px;}')
		self.verticalScrollBar().setStyleSheet('QScrollBar {height:0px;}')

		content = QWidget(self)
		self.setWidget(content)

		layout = QVBoxLayout(content)

		self.label = QLabel(content)

		self.label.setAlignment(Qt.AlignLeft | Qt.AlignTop)
		self.label.setStyleSheet('QLabel {margin-left: 0px;}')
		self.label.setMargin(5)
		self.label.setFont(QFont('Arial', 11))
		self.label.setWordWrap(True)

		layout.addWidget(self.label)
		layout.setContentsMargins(0, 0, 0, 0)

	def setText(self, text):
		self.label.setText(text)
