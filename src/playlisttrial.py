import os, sys
from pickle import TRUE
import random
from settings import *
from audio_functions import *
from functools import partial
import requests
import shutil
import db_settings 
from pathlib import Path
import PyQt5
import eyed3
from custom_widgets import *
from PyQt5.QtCore import QUrl, Qt
from PyQt5.QtMultimedia import *
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets
from PyQt5 import QtCore

from PyQt5 import QtWidgets
import PyQt5.QtCore as C
import PyQt5.QtMultimedia as M
import os
from PyQt5.QtWidgets import QStyle, QListWidget, QAction, QInputDialog, QLineEdit



class PyMono(QWidget):
	def __init__(self):
		super().__init__()
		# -- Initializing important functions such as title, size of the window --
		# -- and where the songs will be fetched from --
		db_settings.initDatabaseMain()
		self.setWindowTitle('PyMono')
		self.setGeometry(0, 0, 1440, 720)
		#self.setMinimumSize(1000, 500)
		self.setFixedSize(1440, 720)
		l_back = '#222831'
		self.song_list = []
		self.url = QUrl()
		self.player = QMediaPlayer(self)
		self.playlist = QMediaPlaylist(self.player)
		self.playlist.setPlaybackMode(QMediaPlaylist.Sequential)
		self.player.setPlaylist(self.playlist)
		self.player.setVolume(100)

	# -- Tells the user that a restart is needed when updating library
		self.needs_restart = QLabel(self)
		self.needs_restart.setStyleSheet('QLabel {color: white;}')
		self.needs_restart.setFont(QFont('Arial', 9))
		self.needs_restart.setGeometry(1100, 20, 200, 30)

	# -- Left Dark Label -- Color code: #edf7fa
		self.random_label = QLabel(self)
		self.random_label.setGeometry(0, 0, 310, 720)
		self.random_label.setFixedSize(310, 720)
		self.random_label.setStyleSheet('QLabel {background-color:' + l_back + ';}')

	# -- Playlist label --
		self.label = QListWidget()

	# -- List the songs in the library and lets you doubleclick one to start --
		self.songList = QListWidget(self)
		self.songList.setGeometry(330, 85, 400, 540)
		self.songList.setStyleSheet('''QListWidget {background-color:
										#464B56; color: white;}''')
		self.songList.setFont(QFont('Arial', 13))
		self.vScrollBar = self.songList.verticalScrollBar()
		self.hScrollBar = self.songList.horizontalScrollBar()
		self.vScrollBar.setStyleSheet('QScrollBar {height:0px;}')
		self.hScrollBar.setStyleSheet('QScrollBar {height:0px;}')
		self.updateSongList()
		self.songList.openPersistentEditor(self.songList.currentItem())
		self.songList.doubleClicked.connect(self.chooseSong)

	# -- Initializing player and setting audio to the first song in the library --
		self.setAudio()
		self.player.durationChanged.connect(self.on_dur_change)
		self.player.positionChanged.connect(self.on_pos_change)

	# -- PyMono logo --
		self.logo = QLabel(self)
		self.logo.setStyleSheet('QLabel {background-color:' + l_back +';}')
		self.logo_pixmap = QPixmap(f'{IMG}/Pymono.png')
		self.logo.setPixmap(self.logo_pixmap)
		self.logo.setScaledContents(True)
		self.logo.setGeometry(45, 12, 220, 60)

	# -- Play/Pause button --
		self.play_btn = QPushButton('Play', self)
		self.play_btn.setGeometry(130, 660, 50, 40)
		self.play_btn.setStyleSheet('''QPushButton {background-color:
										#222831;color: white;}''')
		self.play_btn.setFont(QFont('Arial', 10))
		self.play_btn.setFixedSize(50, 40)
		self.play_btn.clicked.connect(self.playButton)

	# -- Shuffle button
		self.shuffle_btn = QPushButton('Shuffle', self)
		self.shuffle_btn.setGeometry(385, 660, 55, 40)
		self.shuffle_btn.setStyleSheet('QPushButton {background-color:'\
										 + l_back + '; color: white;}')
		self.shuffle_btn.setFont(QFont('Arial', 10))
		self.shuffle_btn.clicked.connect(self.getRandomSong)

	# -- Volume control slider --
		self.slider_volume = QSlider(Qt.Horizontal, self)
		self.slider_volume.setRange(0, 100)
		self.slider_volume.setValue(100)
		self.slider_volume.valueChanged.connect(self.volumeChange)
		self.slider_volume.setGeometry(1000, 670, 150, 20)

	# -- Displays the title and artist of currently playing audio --
		self.current_playing = QLabel(self)
		self.current_playing.setFrameStyle(0)
		self.current_playing.setFont(QFont('Arial', 10))
		self.current_playing.setStyleSheet('QLabel {background-color:'\
										 	+ l_back + '; color: white;}')
		self.current_playing.setGeometry(22, 635, 260, 20)

	# -- Display current album cover --
		self.album_cover = QLabel(self)
		self.album_cover.setFixedSize(270, 270)
		self.album_cover.setStyleSheet('QLabel {background-color:' + l_back + ';}')
		self.album_cover.setScaledContents(True)
		self.album_cover.setFrameStyle(0)
		self.album_cover.setGeometry(20, 355, 270, 270)

	# -- Unfinished label to sow lyrics of current song --
		self.showLyrics = ScrollableLabel(self)
		self.showLyrics.setGeometry(910, 85, 500, 540)
		self.showLyrics.setStyleSheet('''QLabel {background-color: #464B56;
										color: white;}''')
		self.showLyrics.setAlignment(Qt.AlignTop)
		self.showLyrics.setFont(QFont('Arial', 20))

	# -- Artist information label --
		self.song_info = ScrollableLabel(self)
		self.song_info.setGeometry(20, 85, 270, 260)
		self.song_info.setStyleSheet(''' QLabel {background-color: #393e46;
										color: white;}''')
		self.song_info.setAlignment(Qt.AlignTop)

	# -- Displays progress bar without rewind function --
		self.slider_progress = QSlider(Qt.Horizontal, self)
		self.slider_progress.setRange(0, self.player.duration() // 1000)
		self.slider_progress.setValue(1000)
		self.slider_progress.valueChanged.connect(self.progressChange)
		self.slider_progress.setGeometry(460, 670, 300, 20)
		self.slider_progress.setStyleSheet('''QSlider::handle:horizontal
		{border: 10px;}''')
		#self.slider_progress.setRange(0, 100)
		#self.slider_progress.setEnabled(False)
		#self.slider_progress.setMouseTracking(True)
		#self.slider_progress.setPageStep(10)
		#self.slider_progress.setSliderPosition(0)
		#self.slider_progress.setOrientation(QtCore.Qt.Horizontal)
		#self.slider_progress.setInvertedControls(False)
		#self.slider_progress.setObjectName("TimeSlider") ##NOT DONE

	# -- Button to change song to the next one in the list --
		self.next_btn = QPushButton('Next', self)
		self.next_btn.setGeometry(190, 664, 45, 30)
		self.next_btn.setFont(QFont('Arial', 10))
		self.next_btn.clicked.connect(self.nextButton)

	# -- Button to change song to the previous one in the list --
		self.prev_btn = QPushButton('Prev', self)
		self.prev_btn.setGeometry(75, 664, 45, 30)
		self.prev_btn.setFont(QFont('Arial', 10))
		self.prev_btn.clicked.connect(self.prevButton)


		self.browse_files = QPushButton('Browse File', self)
		self.browse_files.setStyleSheet('''QPushButton {color: white;}''')
		self.browse_files.setGeometry(1170, 20, 110, 40)
		self.browse_files.setFont(QFont('Arial', 11))
		self.browse_files.clicked.connect(self.select_file)

	# -- Browse directory button --
		self.updateLib_btn = QPushButton('Browse Folder', self)
		self.updateLib_btn.setGeometry(1300, 20, 110, 40)
		self.updateLib_btn.setStyleSheet('QPushButton {background-color:'\
										 + l_back + '; color: white;}')
		self.updateLib_btn.setFont(QFont('Arial', 11))
		self.updateLib_btn.setFlat(False)
		self.updateLib_btn.clicked.connect(self.select_dir)

	# -- Playlist trial  ONGOING 	
		self.playlist_btn = QPushButton('Add', self)
		self.playlist_btn.setGeometry(20, 664, 45, 30)
		self.playlist_btn.setFont(QFont('Arial', 11))
		self.playlist_btn.clicked.connect(self.click)### ADD FUNC
		#self.all_playlists = self.load_playlists()
		
		#self.l_playlists = QtWidgets.QLabel("Playlists:")
		self.viewPlaylists = QListWidget()
		self.playlists_scroll_area = QtWidgets.QScrollArea()
		self.playlists_scroll_area.setWidget(self.viewPlaylists)
		self.playlists_scroll_area.setWidgetResizable(True)

		self.l_playlists = QPushButton("Playlists")
		self.l_playlists.setGeometry(210, 664, 45, 30)
		self.l_playlists.setFont(QFont('Arial', 11))
		self.l_playlists.clicked.connect(self.load_playlists) ##ONGOING


		#self.l_playlists = QtWidgets.QLabel("Playlists:")
		self.all_playlists = self.load_playlists()
		
		self.view = QListWidget()
		self.viewPlaylists = QListWidget()
		self.all_playlists = self.load_playlists()
		self.viewPlaylists.adjustSize()

		self.all_playlists = self.load_playlists()

		self.view.itemDoubleClicked.connect(self.click)
		self.view.installEventFilter(self)
		

	# -- Connects index change for playlist to a function --
		self.playlist.currentIndexChanged.connect(partial(self._update_index))

	def open_folder(self):
		target = QFileDialog.getExistingDirectory()
		return target

	def select_dir(self):
		target = Path(self.open_folder())
		cache_list = []
		for i in target.glob('*'):
			if self.isAudioFile(i):
				with LIB_FUNC.open(mode='r', encoding='utf-8') as file:
					for j in file.readlines():
						cache_list.append(j.replace('\n', ''))

				if not str(i) in cache_list:
					with LIB_FUNC.open(mode='a', encoding='utf-8') as f:
						f.write(str(i))
						f.write('\n')

				if i.suffix == '.mp3':
					shutil.copy(i, LIBRARY)
				else:
					getInfoFromShazam(i)

		self.playlist.clear()
		self.songList.clear()
		self.updateSongList()
		print('Done')

	def open_files(self):
		target = QFileDialog.getOpenFileName()
		return list(target)[0]

	def select_file(self):
		target = Path(self.open_files())
		cache_list = []
		if self.isAudioFile(target):
			with LIB_FUNC.open(mode='r', encoding='utf-8') as file:
				for i in file.readlines():
					cache_list.append(i.replace('\n', ''))

			if not str(target) in cache_list:
				with LIB_FUNC.open(mode='a', encoding='utf-8') as f:
					f.write(str(target))
					f.write('\n')

				if target.suffix == '.mp3':
					shutil.copy(target, LIBRARY)
				else:
					getInfoFromShazam(target)

			self.playlist.clear()
			self.songList.clear()
			self.updateSongList()
			print('Done')
		else:
			print('Invalid file format... Must be audio file')

	def isAudioFile(self, file):
		if file.suffix == '.mp3':
			return True
		elif file.suffix == '.wav':
			return True
		elif file.suffix == '.mp4':
			return True
		else:
			return False

	def getSongInformation(self):
		if self.song is not None:
			self.song = self.currentSong()
			stripped = self.song.replace('/', '')
			name = eyed3.load(f'{LIBRARY}/{stripped}.mp3').tag
			year = self.getReleaseYear(self.song)
			self.song_info.setText(f'''Title: {name.title}

Artist: {name.artist}

Album: {name.album}

Release year: {year}''')


	def _update_index(self):
		self.songList.setCurrentRow(self.playlist.currentIndex())
		self.song = self.currentSong()
		self.songChanged(self.song)
		self.getCurrentAlbum(self.song)
		self.getLyrics(self.song)
		self.getSongInformation()
		print('e')
		if self.playlist.currentIndex() < 0:
			self.songList.setCurrentRow(0)
			self.playlist.setCurrentIndex(0)

	def nextButton(self):
		self.playlist.setCurrentIndex(self.songList.currentRow())
		self.playlist.next()
		self.player.play()

	def update_position(self, position):

		def hhmmss(ms):
			h, r = divmod(ms, 360000)
			m, r = divmod(r, 60000)
			s, _ = divmod(r, 1000)
			return ("%d:%02d:%02d" % (h, m, s)) if h else ("%d:%02d" % (m, s))
		
		try:
			if position >= 0: self.slider_progress.setText(hhmmss(position)) 
			muteStatus = self.player.isMuted()
			self.slider_progress.blockSignals(True)
			self.player.setMuted(True)  
			
			self.slider_progress.setValue(position)
			self.player.setMuted(muteStatus)  
			self.slider_progress.blockSignals(False)
		except Exception as err:
			print("Error in PyMono - update_position(): ", err)
	
	def prevButton(self):
		self.playlist.setCurrentIndex(self.songList.currentRow())
		self.playlist.previous()
		self.player.play()

	def currentSong(self):
		if self.songList.currentItem() is not None:
			return self.songList.currentItem().text()
    
	def updateSongList(self):
		for i in LIBRARY.glob('*.mp3'):

			if_name = i.stem
			name = eyed3.load(i).tag
			if not name.title:
				self.playlist.addMedia(QMediaContent(self.url.fromLocalFile(str(i))))
				self.songList.addItem(f'{if_name}')
			else:
				self.playlist.addMedia(QMediaContent(self.url.fromLocalFile(str(i))))
				self.songList.addItem(f'{name.title} - {name.artist}')

	def on_dur_change(self, length):
		self.slider_progress.setMaximum(length)

	def on_pos_change(self, position):
		self.slider_progress.setValue(position)

	def progressChange(self):
		progress = self.slider_progress.sliderPosition()
		self.player.setPosition(progress)

	def songChanged(self, song):
		self.current_playing.setText(f'Now playing: {song}')
		self.play_btn.setText('Pause')

	def setAudio(self):
		if list(LIBRARY.rglob('*mp3')):
			self.playlist.setCurrentIndex(0)
			self.songList.setCurrentRow(0)
			self.song = self.currentSong()
		else:
			self.updateLibrary()

	def chooseSong(self):
		self.playlist.setCurrentIndex(self.songList.currentRow())
		self.playlist.setPlaybackMode(QMediaPlaylist.Sequential)
		self._update_index()
		self.player.play()

	def getRandomSong(self):
		rndm = random.randint(0, self.songList.count() - 1)
		self.playlist.setPlaybackMode(QMediaPlaylist.Random)
		self.playlist.setCurrentIndex(rndm)
		self._update_index()
		self.player.play()

	def playButton(self):
		if self.player.state() in (0, 2):
			self.player.play()
			if self.player.state() == 1:
				self.play_btn.setText('Pause')
				self.current_playing.setText(f'Now playing: {self.song}')
				self.getCurrentAlbum(self.song)
				self.getLyrics(self.song)
				self.getSongInformation()
		else:
			self.player.pause()
			self.current_playing.setText('')
			self.play_btn.setText('Play')

	def getCurrentArtist(self):
		id3 = eyed3.load(self.song).tag
		return f'{id3.title} - {id3.artist}'

	def volumeChange(self):
		volume = self.slider_volume.value()
		self.player.setVolume(volume)

	def initLibrary(self):
		if list(LIBRARY.rglob('*')):
			for i in LIBRARY.glob('*'):
				name = str(i)[str(i).rfind('/') + 1:str(i).rfind('.')]
				self.song_list.append(name)
		else:
			self.updateLibrary()

	def updateLibrary(self):
		pass

	def click(self, item): ##ongoing
		self.current_playing = item
		menu = QtWidgets.QMenu()
		createPlaylist = QAction('Create New Playlist')
		createPlaylist.triggered.connect(self.create_playlist)
		menu.addAction(createPlaylist)
		addToPlaylist = QAction('Add To Playlist')
		addToPlaylist.triggered.connect(self.add_to_playlist)
		menu.addAction(addToPlaylist)
		menu.exec_()
	
	def create_playlist(self):
		root = C.QFileInfo(__file__).absolutePath()
		spot = (root + '/playlists/')
		playlistName = self.getText()
		completeName = os.path.join(spot, f'{playlistName}.m3u')
		file = open(completeName, 'w')
		file.close()
		#self.all_playlists.clear()
		self.playlists_scroll_area.update()
		self.all_playlists = self.load_playlists()
		self.playlists_scroll_area.update()
	

	def getText(self):
		text, okPressed = QInputDialog.getText(self, "New Playlist", "Playlist Name:", QLineEdit.Normal, "")
		if okPressed and text != '':
			return text

	def add_to_playlist(self):
		menu = QtWidgets.QMenu()
		for item in self.all_playlists:
			menuItemTask = menu.addAction(item)
			menuItemTask.triggered.connect(self.picked_playlist)
			menu.addAction(menuItemTask)
		menu.exec_()

	def picked_playlist(self):
		print(self.current_playing)
	
	def load_playlists(self): #ANOTHER TRIAL
		playlists = []
		root = C.QFileInfo(__file__).absolutePath()
		songs = os.listdir(root + "/playlists")
		for item in songs:
			if str(item[-4:]) == '.m3u':
				self.viewPlaylists.addItem(item[:-4])
				print(root + "/songs" + item)
				playlists.append(root + "/songs" + item)
			return playlists

	def getReleaseYear(self, song):
		item = song.replace('/', '')
		id3 = eyed3.load(f'{LIBRARY}/{item}.mp3').tag
		artist = str(id3.artist).replace(' ', '_')
		title = str(id3.title).replace(' ', '_')
		json_file = SONGS_JSON / f'{title}-{artist}.json'
		if json_file.exists():
			with open(json_file) as f:
				data = json.load(f)

			try:
				return data['track']['sections'][0]['metadata'][2]['text']
			except KeyError:
				return None
			except IndexError:
				return None

	def getLyrics(self, song):
		if not song is None:
			item = song.replace('/', '')
			id3 = eyed3.load(f'{LIBRARY}/{item}.mp3').tag
			artist = str(id3.artist).replace(' ', '_')
			title = str(id3.title).replace(' ', '_')
			json_file = SONGS_JSON / f'{title}-{artist}.json'
			if json_file.exists():
				with open(json_file) as f:
					data = json.load(f)

				try:
					lyrics = data['track']['sections'][1]['text']
					if lyrics:
						lyrics_string = ''
						for i in lyrics:
							lyrics_string += f'{i}\n'

						self.showLyrics.setText(lyrics_string)
				except KeyError:
					self.showLyrics.setText('No lyrics found...')
				except IndexError:
					self.showLyrics.setText('No lyrics found...')
			else:
				self.showLyrics.setText('No lyrics found...')


	def getCurrentAlbum(self, song):
		if song is not None:
			item = song.replace('/', '')
			curr_song = eyed3.load(f'{LIBRARY}/{item}.mp3').tag

			picture_url = getAlbumCover(curr_song.album)
			if picture_url:
				filename = f'{curr_song.album}-{picture_url.split("/")[-1]}'
				path_to_file = IMG / filename
				r = requests.get(picture_url, stream=True)

				if r.status_code == 200:
					r.raw.decode_content = True

					if not Path(f'{IMG}/{filename}').exists():
						with open(f'{IMG}/{filename}', 'wb') as f:
							shutil.copyfileobj(r.raw, f)

						self.album_cover_pixmap = QPixmap(str(path_to_file))
						self.album_cover.setPixmap(self.album_cover_pixmap)
					else:
						self.album_cover_pixmap = QPixmap(str(path_to_file))
						self.album_cover.setPixmap(self.album_cover_pixmap)
				else:
					print('Image could not be downloaded')
			else:
				self.album_cover_pixmap = QPixmap(f'{IMG}/Placeholder.png')
				self.album_cover.setPixmap(self.album_cover_pixmap)


if __name__ == '__main__':
	app = QApplication(sys.argv)
	app.setStyle('Fusion')
	window = PyMono()
	window.setStyleSheet('QWidget {background-color: #393e46;}')
	window.show()

	try:
		sys.exit(app.exec())
	except SystemExit:
		print('Closing...')