from settings import *
from db_settings import *
import random
from pathlib import Path
from pydub import AudioSegment
import eyed3
import os
from ShazamAPI import Shazam
import json


def getInfoFromShazam(path_to_song):
	song = getOnlyFileName(path_to_song)
	file_name = str(song)[:str(song).rfind('.')]
	song_format = str(path_to_song)[str(path_to_song).rfind('.') + 1:]
	
	# Creates a file to get info from shazam api
	file_to_rec = open(path_to_song, 'rb').read()		
	shazam = Shazam(file_to_rec)
	rec_generator = shazam.recognizeSong()

	# Creates a temporary json file to save info about song
	temp_json = SONGS_JSON / 'temp.json'
	temp_json.touch()
	print('Loading...')

	# Creates dump of jason data and takes temp variables to use to write json file
	json_data = json.dumps(next(rec_generator))
	first_curly = json_data.find('{')
	last_curly = json_data.rfind('}')

	# Creates a connection with temp_json and writes information from json dump
	with temp_json.open(mode='w', encoding='utf-8') as f:
		f.write(json_data[first_curly:-1])

	# Creates a connection with temp_json and reads as a dict (data)
	with open(temp_json) as j:
		data = json.load(j)

	# Takes song name and artist to rename temp_json to "name_artist.json"
	if data['matches']:
		song_artist = data['track']['subtitle'].replace(' ', '_').replace('/', '')
		song_name = data['track']['title'].replace(' ', '_').replace('/', '')

		# Send into database
		try:
			artist_id = int(data['track']['artists'][0]['id'])
		except KeyError:
			artist_id = 0
		except IndexError:
			artist_id = 0

		clean_song_name = data['track']['title']
		clean_artist_name = data['track']['subtitle']
		try:
			album_name = data['track']['sections'][0]['metadata'][0]['text']
			album_year = data['track']['sections'][0]['metadata'][2]['text']
		except KeyError:
			album_name = 'Unknown'
			album_year = 'Unknown'
		except IndexError:
			album_name = 'Unknown'
			album_year = 'Unknown'

		artist_art = data['track']['images']['background']
		cover_art = data['track']['images']['coverart']
		
		getArtistInfo(artist_id, clean_artist_name, artist_art)
		if getAlbumInfo(artist_id, album_name):
			insertAlbumSQL(artist_id, album_name, album_year, 0, cover_art)
		else:
			pass
			# print('Album already in database...')
		if getTrackInfo(clean_song_name, artist_id):
			insertTrackSQL(clean_song_name, artist_id, album_name, album_year)
		else:
			pass
			# print('Track already in database...')

		final_json = f'{song_name}-{song_artist}.json'
		if Path(SONGS_JSON / f'{song_name}-{song_artist}.json').exists():
			temp_json.unlink()
			#print(f'{song_name}-{song_artist}.json already exists.. Not replacing..')
			convertToMp3(path_to_song, clean_song_name, final_json, clean_artist_name)
		else:
			temp_json.replace(SONGS_JSON / final_json)
			#print(f'Created {song_name}-{song_artist}.json')
			#print('Converting song to mp3 and adding to library...')
			convertToMp3(path_to_song, clean_song_name, final_json, clean_artist_name)
	else:
		#print('Could not find matches in database... Converting to mp3...')
		temp_json.unlink()
		convertToMp3_IfNoMetadata(path_to_song, song_format)


def convertToMp3_IfNoMetadata(path_to_song, _format):
	song = getOnlyFileName(path_to_song)
	AudioSegment.from_file(path_to_song, format=_format).export(f'{LIBRARY}/{song} - Unknown Artist.mp3', format='mp3')


def convertToMp3(path_to_song, song_name, json_file, clean_artist_name=None):

	song = getOnlyFileName(path_to_song)
	print(f'Fetching metadata...')

	song_format = str(path_to_song)[str(path_to_song).rfind('.') + 1:]
	song_name = song_name.replace('/', '')
	song_mp3 = Path(LIBRARY / f'{song_name} - {clean_artist_name}.mp3')
	AudioSegment.from_file(path_to_song, format=song_format).export(song_mp3, format='mp3')
	getMetaData(song_mp3, json_file)
	

def getMetaData(path_to_song, json_file):

	id3_path = Path(path_to_song)
	id3 = eyed3.load(id3_path).tag
	only_name = getOnlyFileName(path_to_song)
	json_path = Path(f'{SONGS_JSON}/{json_file}')
	with open(json_path) as f:
		data = json.load(f)

	id3.artist = data['track']['subtitle']
	id3.title = data['track']['title']
	try:
		id3.album = data['track']['sections'][0]['metadata'][0]['text']
	except KeyError:
		id3.album = None
	except IndexError:
		id3.album = None
	id3.save()
	

def getOnlyFileName(full_path):
	return str(full_path)[str(full_path).rfind('/') + 1:str(full_path).rfind('.')]


def getRandomArtistID(artist_name):
	id = random.randint(0, 999999)
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute(f'''SELECT artist.name FROM artist 
						WHERE artist.name = {artist_name}''')
		response = cur.fetchall()
		if not response:
			return id
		else:
			return 'Artist already exists in database'
	except Error as er:
		print(er)


def getArtistInfo(artist_id, artist_name, artist_art=None):
	conn = create_connection(DB_FILE)
	try:
		if artist_id == 0:
			artist_id = getRandomArtistID(artist_name)

		cur = conn.cursor()
		cur.execute('''SELECT artist.id, artist.name FROM artist;''')
		response = cur.fetchall()
		if len(response) == 0:
			insertArtistSQL(conn, artist_id, artist_name, artist_art)
		for i in response:
			if list(i)[0] == artist_id:
				print('Artist already exists')
			else:
				insertArtistSQL(conn, artist_id, artist_name, artist_art)
	except Error as er:
		print(er)
	

def insertArtistSQL(conn, artist_id, artist_name, artist_art=None):
	try:
		cur = conn.cursor()
		cur.execute(f'''INSERT INTO artist(id, name, art)
							VALUES("{artist_id}","{artist_name}","{artist_art}")''')
		conn.commit()
	except Error as er:
		print(er)


def getAlbumInfo(artist_id, album_name):
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute(f'''SELECT album.name, album.artist_id FROM album
							WHERE album.name = "{album_name}" 
							AND album.artist_id = "{artist_id}"''')
		response = cur.fetchall()
		if not response:
			return True
		for i in response:
			if list(i)[0] == album_name and int(list(i)[1]) == int(artist_id):
				return False
			else:
				return True
	except Error as er:
		print(er)


def insertAlbumSQL(artist_id, album_name, album_year, aot=0, art=None):
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute(f'''INSERT INTO album(name, artist_id, year, amount_of_tracks, art)
				VALUES("{album_name}","{artist_id}","{album_year}","{aot}","{art}")''')
		conn.commit()
	except Error as er:
		print(er)


def getTrackInfo(song_name, artist_id):
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute('''SELECT track.name, track.artist_id FROM track;''')
		track = cur.fetchall()
		if not track:
			return True
		for i in track:
			if i[0] == song_name and i[1] == artist_id:
				return False
			else:
				return True
	except Error as er:
		print(er)


def getAlbumID(artist_id, album_name):
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute(f'''SELECT album.id FROM album 
						WHERE album.name = "{album_name}" 
						AND album.artist_id = "{artist_id}"''')

		album_id = cur.fetchall()
		return list(album_id[0])[0]
	except Error as er:
		print(er)


def insertTrackSQL(track_name, artist_id, album_name, album_year):
	conn = create_connection(DB_FILE)
	album_id = getAlbumID(artist_id, album_name)
	try:
		cur = conn.cursor()
		cur.execute(f'''INSERT INTO track(name, artist_id, album_id, year)
			VALUES("{track_name}","{artist_id}","{album_id}","{album_year}")''')
		conn.commit()
	except Error as er:
		print(er)


def getAlbumCover(album_name):
	conn = create_connection(DB_FILE)
	try:
		cur = conn.cursor()
		cur.execute(f'''SELECT album.art FROM album
						WHERE album.name = "{album_name}"''')
		response = cur.fetchall()
		if response:
			return list(response[0])[0]
	except Error as er:
		print(er)


if __name__ == '__main__':
	pass
